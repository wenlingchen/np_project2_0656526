#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/resource.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <sstream>
#include <csignal>
#include <vector>
#include <netinet/in.h>
#include <netdb.h>
#define MAXPATH 200
#define MAXLINE 1024
#define MAXPARLEN 20
#define MAXPAR 20
#define QLEN 30	//maximum connection queue length

void initialize_path();
void send_welcome_msg(int sockfd);
void get_conn_msg(int sockfd,char * user_name,char * user_ip,int user_port);
void send_symbol(int sockfd);
int readline(int fd, char * ptr,int maxlen);
//void set_pipe();
void signal_handler(int signum);
void broadcast(int * user_fd_table,int tablesize,char * msg);
void write2(int sockfd,char *msg);
char * int2char(int i);
int passivesock(char * service,char * protocol,int qlen);
int echo(int fd);
using namespace std;

struct rlimit r;	//pipe_limit
struct client
{
	int sockfd[QLEN];
	char name[QLEN][20];
	char ip[QLEN][16];
	int port[QLEN];
	char envp[QLEN][50];
};
struct pipe_cmd	//PTable
{
	int num;
	char * cmd[MAXPAR];
};
struct waiting_pipe	//WTable
{
	int owner;
	int num;
	int fd[2];
};

int main(int argc, char *argv[])
{
	signal(SIGINT,signal_handler);	//set signal handler: SIGINT=ctrl+C
	//set_pipe();
	clearenv();	//clear environment variable
	initialize_path();	//PATH=bin:.

	char user_name[QLEN][20];	//max length of name is 20
	int user_fd[QLEN];
	char user_envp[QLEN][50][50];	//50 parameter, maxlen=50
	int user_envp_size[QLEN];
	char user_ip[QLEN][30];
	int user_port[QLEN];
	int user_pipe[QLEN][QLEN];	//from,to,flag 30*30 (sender,reciever)

	struct sockaddr_in user_fsin[QLEN];
	//Initialize fd and pipe
	for(int i=0;i<QLEN;i++)
	{
		user_fd[i]=-1;
		for(int j=0;j<QLEN;j++)
		{
			user_pipe[i][j]=0;
		}
	}
	///////////////Create WTable///////////////
	waiting_pipe WTable[1000];
	int waiting_counter=0;
	int unknowncmd_flag=0;	//check close or not

	//ppt page.34 start
	char * service;
	struct sockaddr_in fsin;	//the from address of a client
	int msock;
	fd_set afds;	//active file descriptor set
	fd_set rfds;	//read file descriptor set
	
	int fd,nfds;
	switch(argc)
	{
		case 1:
			break;
		case 2:
			service=argv[1];
			break;
		default:
			cout<<"usage: TCPmechod [port]\n";
			exit(1);
	}
	//ppt page.36 start
	msock=passivesock(service,"tcp",QLEN);
	nfds=getdtablesize();
	FD_ZERO(&afds);
	FD_SET(msock,&afds);
	while(1)
	{
		memcpy(&rfds,&afds,sizeof(rfds));
		if(select(nfds,&rfds,(fd_set *)NULL,(fd_set *)NULL,(struct timeval *)NULL)<0)
		{
			cout<<"select error\n";
			exit(1);
		}
		//New connection
		if(FD_ISSET(msock,&rfds))
		{
			int alen;	//from-address length
			int uid;
			for(int i=0;i<QLEN;i++)	//minimum uid
			{
				if(user_fd[i]==-1)	//NULL
				{
					uid=i;
					break;
				}
			}
			int ssock;
			alen=sizeof(user_fsin[uid]);
			ssock=accept(msock,(struct sockaddr *)&(user_fsin[uid]),(socklen_t*)&alen);	//someone connect to server
			if(ssock<0)
			{
				cout<<"accept error\n";
				exit(1);
			}
			else
			{
				//cout<<nfds<<endl;
				user_fd[uid]=ssock;
				strcpy(user_name[uid],"(no name)");
				//strcpy(user_ip[uid],inet_ntoa(user_fsin[uid].sin_addr));
				//user_port[uid]=(int)ntohs(user_fsin[uid].sin_port);
				strcpy(user_ip[uid],"CGILAB");
				user_port[uid]=511;

				user_envp_size[uid]=0;
				strcpy(user_envp[uid][user_envp_size[uid]],"PATH=bin:.");	//set env PATH
				user_envp_size[uid]++;	//1

				//cout<<inet_ntoa(user_fsin[uid].sin_addr)<<"\t"<<(int)ntohs(user_fsin[uid].sin_port)<<endl;
				
				FD_SET(ssock,&afds);
				
				send_welcome_msg(ssock);

				char conn_msg[100];
				char port[10];
				sprintf(port,"%d",user_port[uid]);
				strcpy(conn_msg,"*** User '");
				strcat(conn_msg,user_name[uid]);
				strcat(conn_msg,"' entered from ");
				strcat(conn_msg,user_ip[uid]);
				strcat(conn_msg,"/");
				strcat(conn_msg,port);
				strcat(conn_msg,". ***\n");
				broadcast(user_fd,QLEN,conn_msg);	//broadcast to everyone
				memset(conn_msg,0,sizeof(conn_msg));
				
				send_symbol(ssock);	//%
			}

		}
		//Process client input
		for(fd=0;fd<nfds;++fd)	//maximum=40 (>30+4?)
		{
			if(fd!=msock && FD_ISSET(fd,&rfds))
			{
				//get current uid
				int cuid;
				for(int i=0;i<QLEN;i++)
				{
					if(user_fd[i]==fd)
					{
						cuid=i;
						break;
					}
				}
				clearenv();
				for(int i=0;i<user_envp_size[cuid];i++)
				{
					putenv(user_envp[cuid][i]);	//set client's envp
				}
				int n;
				char line[MAXLINE];	//MAX input lenth:10000 char
				memset(line,0,MAXLINE);
				//read input from client
				n=readline(fd,line,MAXLINE);
				string line_str=line;

				cout<<"fd="<<fd<<"\t"<<line;

				char cli_cmd_par[MAXPARLEN];	//each parameter(20 char)
				pipe_cmd PTable[100];	//MAX command amount for each line
				for(int i=0;i<100;i++)
				{
					for(int j=0;j<MAXPAR;j++)	//20
					{
						PTable[i].cmd[j]=new char [MAXPARLEN];	//new 2nd dimension:MAXPARLEN
						memset(PTable[i].cmd[j],0,MAXPARLEN);//Initial set null
					}
				}
				int pipe_counter=0;
				int cmd_counter=0;
				int end_flag=0; //record EOF: cmd or |num
				/////////////Parse and Save input to PTable/////////////
				stringstream ss;
				ss<<line_str;
				while(ss>>cli_cmd_par)	//Split with whitespace
				{
					if(cli_cmd_par[0]!='|')	//cmd
					{
						PTable[pipe_counter].num=0;	//set defalt
						for(int i=0;i<MAXPARLEN;i++)	//copy by value (each char)
						{
							PTable[pipe_counter].cmd[cmd_counter][i]=cli_cmd_par[i];
						}
						cmd_counter++;
						end_flag=0;
					}
					else	//| or |num
					{
						string str=cli_cmd_par;
						if(str.length()!=1)	//|num
						{
							int num=0;
							for(int i=1;i<str.length();i++)
							{
								if(isdigit(str[i]))
								{
								  num*=10;
								  num+=str[i]-'0';
								}
							}
							PTable[pipe_counter].num=num;
							PTable[pipe_counter].cmd[cmd_counter]=NULL;
							cmd_counter=0;
							pipe_counter++;
							end_flag=1;
						}
						else	//only |
						{
							PTable[pipe_counter].num=1;
							PTable[pipe_counter].cmd[cmd_counter]=NULL;
							cmd_counter=0;
							pipe_counter++;
						}
					}
					//clean every time
					memset(cli_cmd_par,0,sizeof(cli_cmd_par));
				}
				ss.str("");	//Clean SS
				ss.clear();
				//memset(line,0,sizeof(line));	//clean after saved (client input)
				if (end_flag==0)	//END is cmd (need add '\0')
				{
					PTable[pipe_counter].cmd[cmd_counter]=NULL;
					pipe_counter++;
				}
				///////////////////Print PTable///////////////////

				//cout<<"=========New readline========="<<endl;
				//cout<<"Pipe counter:"<<pipe_counter<<endl;
				/*
				for(int i=0;i<pipe_counter;i++)
				{
					cout<<"~~~Pipe cmd "<<i<<"~~~num="<<PTable[i].num<<endl;
					for(int j=0;j<MAXPAR;j++)	//20
					{
						if(PTable[i].cmd[j]!=NULL)
						{
							cout<<PTable[i].cmd[j]<<"\t";
						}
						else{break;}
					}
					cout<<endl;
				}
				*/

				///////////////////who: Print CTable///////////////////
				if(pipe_counter==1 && strcmp(PTable[0].cmd[0],"who")==0)
				{
					char result[MAXLINE];
					strcpy(result,"<ID>\t<nickname>\t<IP/port>\t<indicate me>\n");
					write2(fd,result);
					memset(result,0,sizeof(result));	//clean every time
					for (int i=0;i<QLEN;i++)
					{
						if(user_fd[i]!=-1)	//online user
						{
							char uid_tmp[10],port_tmp[10];
							sprintf(uid_tmp,"%d",i+1);	//convert int to char*
							sprintf(port_tmp,"%d",user_port[i]);
							strcpy(result,uid_tmp);
							strcat(result,"\t");
							strcat(result,user_name[i]);
							strcat(result,"\t");
							strcat(result,user_ip[i]);
							strcat(result,"/");
							strcat(result,port_tmp);
							if(i==cuid)	//current id (me)
							{
								strcat(result,"\t<-me");
							}
							strcat(result,"\n");
							write2(fd,result);
							memset(result,0,sizeof(result));	//clean every time
							memset(uid_tmp,0,sizeof(uid_tmp));
							memset(port_tmp,0,sizeof(port_tmp));
						}
					}
					//memset(result,0,sizeof(result));	//clean every time
				}
				///////////////////exit///////////////////
				else if(pipe_counter==1 && strcmp(PTable[0].cmd[0],"exit")==0)
				{
					for(int i=0;i<QLEN;i++)
					{
						//close(user_pipe[i][cuid][0]);	//close pipe (can't receive)
						//user_pipe[i][cuid][0]=-1;	//initialize

						user_pipe[i][cuid]=0;

						char num_s[10],num_c[10];
						memset(num_s,0,sizeof(num_s));
						memset(num_c,0,sizeof(num_c));
						sprintf(num_s,"%d",i+1);
						sprintf(num_c,"%d",cuid+1);
						char f_tmp[20];
						memset(f_tmp,0,sizeof(f_tmp));
						strcpy(f_tmp,"../");
						strcat(f_tmp,num_s);
						strcat(f_tmp,"to");
						strcat(f_tmp,num_c);
						strcat(f_tmp,".txt");
						remove(f_tmp);
					}

					//broadcast
					char disconn_msg[100];
					strcpy(disconn_msg,"*** User '");
					strcat(disconn_msg,user_name[cuid]);
					strcat(disconn_msg,"' left. ***\n");
					broadcast(user_fd,QLEN,disconn_msg);	//broadcast to everyone in Table
					user_fd[cuid]=-1;
					memset(disconn_msg,'\0',sizeof(disconn_msg));
					FD_CLR(fd,&afds);
					//shutdown(fd,2);
					close(fd);
					
					//continue;	//next fd
				}
				///////////////////name///////////////////
				else if(pipe_counter==1 && strcmp(PTable[0].cmd[0],"name")==0)
				{
					char name[20];
					//check name, can't be same
					int check_flag=0;
					for(int i=0;i<QLEN;i++)
					{
						if(user_fd[i]!=-1 && strcmp(user_name[i],PTable[0].cmd[1])==0)	//online, same
						{
							check_flag=1;
							char name_err_msg[100];
							strcpy(name_err_msg,"*** User '");
							strcat(name_err_msg,user_name[i]);
							strcat(name_err_msg,"' already exists. ***\n");
							write2(fd,name_err_msg);
							memset(name_err_msg,'\0',sizeof(name_err_msg));
							break;
						}
					}
					if(check_flag==0)	//update name
					{
						strcpy(user_name[cuid],PTable[0].cmd[1]);
						char name_msg[100];
						char port_tmp[10];
						sprintf(port_tmp,"%d",user_port[cuid]);
						strcpy(name_msg,"*** User from ");
						strcat(name_msg,user_ip[cuid]);
						strcat(name_msg,"/");
						strcat(name_msg,port_tmp);
						strcat(name_msg," is named '");
						strcat(name_msg,user_name[cuid]);
						strcat(name_msg,"'. ***\n");
						broadcast(user_fd,QLEN,name_msg);
						memset(name_msg,'\0',sizeof(name_msg));
						memset(port_tmp,0,sizeof(port_tmp));
					}
				}
				///////////////////yell///////////////////
				else if(pipe_counter==1 && strcmp(PTable[0].cmd[0],"yell")==0)
				{
					char bro_msg[MAXLINE];
					memset(bro_msg,0,sizeof(bro_msg));
					strcpy(bro_msg,"*** ");
					strcat(bro_msg,user_name[cuid]);
					strcat(bro_msg," yelled ***: ");
					for(int i=1;i<MAXPAR;i++)	//20
					{
						if(PTable[0].cmd[i]!=NULL)
						{
							if(i==1) {strcat(bro_msg,PTable[0].cmd[i]);}	//first
							else 
							{
								strcat(bro_msg," ");	//white space
								strcat(bro_msg,PTable[0].cmd[i]);
							}
						}
						else
						{
							strcat(bro_msg,"\n");	//end
							break;
						}
					}
					broadcast(user_fd,QLEN,bro_msg);
				}
				///////////////////tell///////////////////
				else if(pipe_counter==1 && strcmp(PTable[0].cmd[0],"tell")==0)
				{
					int recv=atoi(PTable[0].cmd[1]);
					if(user_fd[recv-1]==-1)
					{
						char tell_err_msg[MAXLINE];
						memset(tell_err_msg,0,sizeof(tell_err_msg));
						strcpy(tell_err_msg,"*** Error: user #");
						strcat(tell_err_msg,PTable[0].cmd[1]);
						strcat(tell_err_msg," does not exist yet. ***\n");
						write2(fd,tell_err_msg);
					}
					else
					{
						char tell_msg[MAXLINE];
						memset(tell_msg,0,sizeof(tell_msg));
						strcpy(tell_msg,"*** ");
						strcat(tell_msg,user_name[cuid]);
						strcat(tell_msg," told you ***: ");
						char *pch;
						pch=strtok(line," \r\n"); //tell
						pch=strtok(NULL," \r\n"); //who
						pch=strtok(NULL,"\r\n");	//msg all
						strcat(tell_msg,pch);
						strcat(tell_msg,"\n");
						/*
						for(int i=2;i<MAXPAR;i++)	//20
						{
							if(PTable[0].cmd[i]!=NULL)
							{
								if(i==2) {strcat(tell_msg,PTable[0].cmd[i]);}	//first
								else 
								{
									strcat(tell_msg," ");	//white space
									strcat(tell_msg,PTable[0].cmd[i]);
								}
							}
							else
							{
								strcat(tell_msg,"\n");	//end
								break;
							}
						}
						*/
						write2(user_fd[recv-1],tell_msg);	
					}
				}
				///////////////////setenv///////////////////
				else if(pipe_counter==1 && strcmp(PTable[0].cmd[0],"setenv")==0)
				{
					setenv(PTable[0].cmd[1],PTable[0].cmd[2],1);
					//setenv(name,value,1), 1 means overwrite
					char env_str[MAXLINE];
					strcpy(env_str,PTable[0].cmd[1]);
					strcat(env_str,"=");
					strcat(env_str,PTable[0].cmd[2]);
					int check_flag=0;
					for(int i=0;i<user_envp_size[cuid];i++)
					{
						if(strstr(user_envp[cuid][i],PTable[0].cmd[1])!=NULL)	//exist same
						{
							check_flag=1;
							strcpy(user_envp[cuid][i],env_str);	//update
							break;
						}
					}
					if(check_flag==0)	//create new
					{
						strcpy(user_envp[cuid][user_envp_size[cuid]],env_str);
						user_envp_size[cuid]++;
					}
				}
				///////////////////printenv///////////////////
				else if(pipe_counter==1 && strcmp(PTable[0].cmd[0],"printenv")==0)
				{
					char result[60];
					strcpy(result,PTable[0].cmd[1]);
					strcat(result,"=");
					strcat(result,getenv(PTable[0].cmd[1]));
					strcat(result,"\n");
					write2(fd,result);
					memset(result,0,sizeof(result));	//clean every time
				}
				///////////////////cmd and pipe///////////////////
				else
				{
					for(int i=0;i<pipe_counter;i++)
					{
						cout<<"=========["<<i<<"]========="<<endl;
						char filename[30];
						char in_filename[30];
						char out_filename[30];
						memset(filename,0,sizeof(filename));
						memset(in_filename,0,sizeof(in_filename));
						memset(out_filename,0,sizeof(out_filename));
						int file_out=0;	//need to output file or not
						int file_in=0;
						int fd_in=STDIN_FILENO;
						int fd_out=STDOUT_FILENO;
						int in_index,out_index;
						FILE *in_stream;
						int tuid=0;	//target uid (>num)
						int suid=0;	//source uid (<num)
						int user_in=0;	//recv from other client
						int user_out=0;	//tell to other client
						char sr_msg[150];	//sort
						char s_tmp[100];	//send
						char r_tmp[100];	//resv
						int file_check=0;
						memset(sr_msg,0,sizeof(sr_msg));
						memset(s_tmp,0,sizeof(s_tmp));
						memset(r_tmp,0,sizeof(r_tmp));
						//(0)Check filename first
						for(int j=0;j<MAXPAR;j++)
						{
							if(PTable[i].cmd[j]==NULL){break;}
							else
							{
								if(strcmp(PTable[i].cmd[j],">")==0)	// > filename
								{
									file_out=1;
									strcpy(filename,PTable[i].cmd[j+1]);
									PTable[i].cmd[j]=NULL;
									PTable[i].cmd[j+1]=NULL;
									//break;
								}
								else if(strchr(PTable[i].cmd[j],'>')!=NULL)	// >num
								{
									cout<<">>>>>>>>>>\n";
									user_out=1;
									//file_out=1;
									string str=PTable[i].cmd[j];
									for(int k=1;k<str.length();k++)	//start from num(1)
									{
										if(isdigit(str[k]))
										{
										  tuid*=10;
										  tuid+=str[k]-'0';
										}
									}
									tuid-=1;	//index

									char num_t[10],num_c[10];
									memset(num_t,0,sizeof(num_t));
									memset(num_c,0,sizeof(num_c));
									sprintf(num_t,"%d",tuid+1);
									sprintf(num_c,"%d",cuid+1);
									
									if(user_fd[tuid]==-1)	// not online
									{
										file_check=1;
										char tell_err_msg[MAXLINE];
										memset(tell_err_msg,0,sizeof(tell_err_msg));
										strcpy(tell_err_msg,"*** Error: user #");
										strcat(tell_err_msg,num_t);
										strcat(tell_err_msg," does not exist yet. ***\n");
										write2(fd,tell_err_msg);
										break;
									}
									else if(user_pipe[cuid][tuid]==1)	//alread exist, cannot write
									{
										file_check=1;
										cout<<"pipe error\n";
										char pipe_err_msg[100];
										memset(pipe_err_msg,0,sizeof(pipe_err_msg));
										strcpy(pipe_err_msg,"*** Error: the pipe #");
										strcat(pipe_err_msg,num_c);
										strcat(pipe_err_msg,"->#");
										strcat(pipe_err_msg,num_t);
										strcat(pipe_err_msg," already exists. ***\n");
										write2(fd,pipe_err_msg);
										break;
									}
									else	//can write
									{
										user_pipe[cuid][tuid]=1;	//change flag
										char f_tmp[20];
										memset(f_tmp,0,sizeof(f_tmp));
										strcpy(f_tmp,"../");
										strcat(f_tmp,num_c);
										strcat(f_tmp,"to");
										strcat(f_tmp,num_t);
										strcat(f_tmp,".txt");
										strcpy(out_filename,f_tmp);	//copy to filename
										cout<<"filename: "<<out_filename<<endl;

										strcpy(s_tmp,"*** ");
										strcat(s_tmp,user_name[cuid]);
										strcat(s_tmp," (#");
										strcat(s_tmp,num_c);
										strcat(s_tmp,") just piped '");
										/*
										for(int m=0;m<pipe_counter;m++)
										{
											for(int k=0;k<MAXPAR;k++)
											{
												if(PTable[m].cmd[k]==NULL){break;}
												else
												{
													if(k!=0){strcat(s_tmp," ");}
													strcat(s_tmp,PTable[i].cmd[k]);
												}
											}
											if(m!=pipe_counter-1){strcat(s_tmp," | ");}
										}
										*/
										char line_copy[MAXLINE];
										memset(line_copy,0,sizeof(line_copy));
										strcpy(line_copy,line);
										char *pch;
										pch=strtok(line_copy,"\r\n");	//msg all
										strcat(s_tmp,pch);
										strcat(s_tmp,"' to ");
										strcat(s_tmp,user_name[tuid]);
										strcat(s_tmp," (#");
										strcat(s_tmp,num_t);
										strcat(s_tmp,") ***\n");
									}
									PTable[i].cmd[j]=NULL;
								}
								else if(strchr(PTable[i].cmd[j],'<')!=NULL)	//<num
								{
									cout<<"<<<<<<<<<\n";
									user_in=1;
									//file_in=1;
									string str=PTable[i].cmd[j];
									cout<<"str="<<str<<endl;
									for(int k=1;k<str.length();k++)	//start from num(1)
									{
										if(isdigit(str[k]))
										{
										  suid*=10;
										  suid+=str[k]-'0';
										}
									}
									suid-=1;	//index
									char num_s[10],num_c[10];
									memset(num_s,0,sizeof(num_s));
									memset(num_c,0,sizeof(num_c));
									sprintf(num_s,"%d",suid+1);
									sprintf(num_c,"%d",cuid+1);
									if(user_pipe[suid][cuid]==0)	//initial state, cannot read
									{
										file_check=1;
										cout<<"pipe error\n";
										char pipe_err_msg[100];
										memset(pipe_err_msg,0,sizeof(pipe_err_msg));
										strcpy(pipe_err_msg,"*** Error: the pipe #");
										strcat(pipe_err_msg,num_s);
										strcat(pipe_err_msg,"->#");
										strcat(pipe_err_msg,num_c); 
										strcat(pipe_err_msg," does not exist yet. ***\n");
										write2(fd,pipe_err_msg);
										break;
									}
									else	//can read
									{
										user_pipe[suid][cuid]=0;	//change flag 1->0
										char f_tmp[20];
										memset(f_tmp,0,sizeof(f_tmp));
										strcpy(f_tmp,"../");
										strcat(f_tmp,num_s);
										strcat(f_tmp,"to");
										strcat(f_tmp,num_c);
										strcat(f_tmp,".txt");
										strcpy(in_filename,f_tmp);	//copy to filename
										cout<<"filename: "<<in_filename<<endl;

										strcpy(r_tmp,"*** ");
										strcat(r_tmp,user_name[cuid]);
										strcat(r_tmp," (#");
										strcat(r_tmp,num_c);
										strcat(r_tmp,") just received from ");
										strcat(r_tmp,user_name[suid]);
										strcat(r_tmp," (#");
										strcat(r_tmp,num_s);
										strcat(r_tmp,") by '");
										/*
										for(int k=0;k<MAXPAR;k++)
										{
											if(PTable[i].cmd[k]==NULL){break;}
											else
											{
												if(k!=0){strcat(r_tmp," ");}
												strcat(r_tmp,PTable[i].cmd[k]);
											}
										}
										*/
										char line_copy[MAXLINE];
										memset(line_copy,0,sizeof(line_copy));
										strcpy(line_copy,line);
										char *pch;
										pch=strtok(line_copy,"\r\n");	//msg all
										strcat(r_tmp,pch);
										strcat(r_tmp,"' ***\n");
									}
									PTable[i].cmd[j]=NULL;
								}
							}
						}

						if(user_in==1) {strcpy(sr_msg,r_tmp);}	//recv first
						else {strcpy(sr_msg,"");}
						if(user_out==1)	{strcat(sr_msg,s_tmp);}
						broadcast(user_fd,QLEN,sr_msg);	//broadcast msg
						if(file_check==1)	//file error
						{
							break;
						}
						///////Check num and create pipe to WTable///////
						//(1)All minus 1
						for(int j=0;j<waiting_counter;j++)
						{
							if(WTable[j].owner==fd)
							{
								WTable[j].num--;
							}
						}
						//(2)Check same or create pipe (stdout) fd_out,out_index
						if(PTable[i].num!=0)
						{
							int tmp=0;
							for(int j=0;j<waiting_counter;j++)
							{
								//same pipe num
								if((WTable[j].owner==fd) && (WTable[j].num==PTable[i].num))
								{
									fd_out=WTable[j].fd[1];
									out_index=j;
									tmp=1;
									break;
								}
							}
							if(tmp==0)	//Not found same, create
							{
								cout<<"[new]"<<endl;
				                WTable[waiting_counter].owner=fd;
				                WTable[waiting_counter].num=PTable[i].num;

				                pipe(WTable[waiting_counter].fd);
				                fd_out=WTable[waiting_counter].fd[1];
				                out_index=waiting_counter;
				                waiting_counter++;
							}
						}
						cout<<"[WTable] "<<waiting_counter<<endl;
						cout<<"<index>\t<owner>\t<num>\n";
						for(int j=0;j<waiting_counter;j++)
						{
							if(WTable[j].num==0)
							{
								cout<<j<<" *\t"<<WTable[j].owner<<"\t"<<WTable[j].num<<endl;
							}
							else
							{
								cout<<j<<"\t"<<WTable[j].owner<<"\t"<<WTable[j].num<<endl;
							}
						}
						if(fd_out!=STDOUT_FILENO){cout<<"@@@@@@"<<endl;}
						else{cout<<"STDOUT"<<endl;}

						//(3)Check zero (stdin)
						if(user_in==1)	//read input from other client
						{
	            			in_stream=fopen(in_filename,"r");
	            			dup2(fileno(in_stream),fd_in);
						}
						else
						{
							for(int j=0;j<waiting_counter;j++)
							{
								if((WTable[j].owner==fd) && (WTable[j].num==0))
								{
									in_index=j;
									fd_in=WTable[j].fd[0];
									//close(WTable[j].fd[1]);
									break;
								}
							}
						}
						if(fd_in!=STDIN_FILENO){cout<<"######"<<endl;}
						else{cout<<"STDIN"<<endl;}
						//(4)exec
						int status;
						pid_t cmd_childpid=fork();
			            if(cmd_childpid<0)
			            {
							perror("fork_pipe");
							exit(1);
			            }
			            //child
			            else if(cmd_childpid==0)
			            {
			            	//not last
			            	if(i!=pipe_counter-1)
			            	{
			            		if(unknowncmd_flag==0)
				            	{
				            		if(fd_in!=STDIN_FILENO){close(WTable[in_index].fd[1]);}
				            	}
			            		dup2(fd_in,0);
			            		if(fd_out!=STDOUT_FILENO){close(WTable[out_index].fd[0]);}
			            		dup2(fd_out,1);
			            		dup2(fd,2);
			            		if(execvp(PTable[i].cmd[0],PTable[i].cmd)!=0)
				                {
				                	char unknown_msg[]="Unknown command: [";
					                strcat(unknown_msg,PTable[i].cmd[0]);
					                strcat(unknown_msg,"].\n");
					                write2(fd,unknown_msg);
					                //write(newsockfd,unknown_msg,strlen(unknown_msg));
					                memset(unknown_msg,0,sizeof(unknown_msg));
					                if(fd_in!=STDIN_FILENO){close(fd_in);}
				                	close(fd_out);	//must
					                exit(3);
				                }
				                else
				                {
				                	if(fd_in!=STDIN_FILENO){close(fd_in);}
				                	close(fd_out);	//must
				                	exit(1);
				                }
			            	}
			            	//last: file, |num, socket
			            	else
			            	{
			            		if(unknowncmd_flag==0)
				            	{
				            		if(fd_in!=STDIN_FILENO){close(WTable[in_index].fd[1]);}
				            	}
			            		dup2(fd_in,0);
			            		dup2(fd,2);
			            		//output to other client
			            		if(user_out==1)
			            		{
			            			FILE *stream;
			            			stream=fopen(out_filename,"w");
			            			dup2(fileno(stream),STDOUT_FILENO);
			            			if(execvp(PTable[i].cmd[0],PTable[i].cmd)!=0)
					                {
										char unknown_msg[]="Unknown command: [";
						                strcat(unknown_msg,PTable[i].cmd[0]);
						                strcat(unknown_msg,"].\n");
						                write2(fd,unknown_msg);
						                //write(newsockfd,unknown_msg,strlen(unknown_msg));
						                memset(unknown_msg,0,sizeof(unknown_msg));
						                fclose(stream);
					                	if(fd_in!=STDIN_FILENO){close(fd_in);}
					                	exit(3);
					                }
					                else
					                {
					                	fclose(stream);
					                	if(fd_in!=STDIN_FILENO){close(fd_in);}
					                	exit(1);
					                }
			            		}
			            		//output to file
			            		else if(file_out==1)
			            		{
			            			freopen(filename,"w",stdout);
			            			if(execvp(PTable[i].cmd[0],PTable[i].cmd)!=0)
					                {
										char unknown_msg[]="Unknown command: [";
						                strcat(unknown_msg,PTable[i].cmd[0]);
						                strcat(unknown_msg,"].\n");
						                write2(fd,unknown_msg);
						                //write(newsockfd,unknown_msg,strlen(unknown_msg));
						                memset(unknown_msg,0,sizeof(unknown_msg));
						                fclose(stdout);
					                	if(fd_in!=STDIN_FILENO){close(fd_in);}
					                	exit(3);
					                }
					                else
					                {
					                	fclose(stdout);
					                	if(fd_in!=STDIN_FILENO){close(fd_in);}
					                	exit(1);
					                }
			            		}	//end of file_out
			            		//output to |num
			            		else if(PTable[i].num!=0)
			            		{
			            			if(fd_out!=STDOUT_FILENO){close(WTable[out_index].fd[0]);}
			            			dup2(fd_out,1);
			            			if(execvp(PTable[i].cmd[0],PTable[i].cmd)!=0)
					                {
										char unknown_msg[]="Unknown command: [";
						                strcat(unknown_msg,PTable[i].cmd[0]);
						                strcat(unknown_msg,"].\n");
						                write2(fd,unknown_msg);
						                //write(newsockfd,unknown_msg,strlen(unknown_msg));
						                memset(unknown_msg,0,sizeof(unknown_msg));
						                if(fd_in!=STDIN_FILENO){close(fd_in);}
				                		close(fd_out);	//must
				                		exit(3);
					                }
					                else
					                {
					                	if(fd_in!=STDIN_FILENO){close(fd_in);}
				                		close(fd_out);
				                		exit(1);
					                }
			            		}	//end of |num
			            		//output to socket (cmd)
			            		else
			            		{
			            			dup2(fd,1);
			            			if(execvp(PTable[i].cmd[0],PTable[i].cmd)!=0)
					                {
					                	char unknown_msg[]="Unknown command: [";
						                strcat(unknown_msg,PTable[i].cmd[0]);
						                strcat(unknown_msg,"].\n");
						                write2(fd,unknown_msg);
						                //write(newsockfd,unknown_msg,strlen(unknown_msg));
						                memset(unknown_msg,0,sizeof(unknown_msg));
						                if(fd_in!=STDIN_FILENO){close(fd_in);}
						                exit(3);
					                }
					                else
					                {
					                	if(fd_in!=STDIN_FILENO){close(fd_in);}
					                	exit(1);
					                }
			            		}	//end of socket
			            	}	//end of last
			            }	//end of child
			            //parent
			            else
			            {
			            	if(user_in==1)
			            	{
			            		fclose(in_stream);
			            		remove(in_filename);
			            	}
			            	if(unknowncmd_flag==0)
			            	{
			            		if(fd_in!=STDIN_FILENO){close(WTable[in_index].fd[1]);}
			            	}
			            	else	//if have unknowncmd, not close again.
			            	{
			            		unknowncmd_flag=0;
			            	}
					        waitpid(cmd_childpid,&status,0);
					        cout<<"STATUS:"<<WEXITSTATUS(status)<<endl;
					        if(WEXITSTATUS(status)==0)	//correct cmd
					        {
					        	if(fd_in!=STDIN_FILENO)
					        	{
					        		close(fd_in);
					        	}
					        }
					        if(WEXITSTATUS(status)==3)	//error cmd
					        {
					        	if(i!=0)
				                {
				                	unknowncmd_flag=1;	//have unknowncmd
				                	for(int j=0;j<waiting_counter;j++)
									{
										if(WTable[j].owner==fd)
										{
											WTable[j].num++;
										}
									}
				                }
				                i=pipe_counter-1;	//move to last one
								cout<<"pipe_counter="<<pipe_counter<<" i="<<i<<endl;
					        }
			            }	//end of parent
						//(5)update WTable
						for(int j=0;j<waiting_counter;j++)
						{
							if(WTable[j].num==0)	//move all
							{
								//waiting_couter is Bound
								for(int k=j+1;k<waiting_counter;k++)
								{
									WTable[k-1].owner=WTable[k].owner;
									WTable[k-1].num=WTable[k].num;
									WTable[k-1].fd[0]=WTable[k].fd[0];
                 					WTable[k-1].fd[1]=WTable[k].fd[1];
								}
								waiting_counter--;
								break;	//only one zero
							}
						}
						cout<<"[Update WTable] "<<waiting_counter<<endl;
						for(int j=0;j<waiting_counter;j++)
						{
							cout<<j<<"\t"<<WTable[j].owner<<"\t"<<WTable[j].num<<endl;
						}

					}	//end of for loop (PTable all cmd)
				}	//end of if (cmd and pipe)					
				

				
				//write(fd,line,strlen(line));	//echo
				send_symbol(fd);	//%

				///////////////////Delete PTable///////////////////
				//Delete PTable.cmd, delete 2 dimension
				for(int i=0;i<100;i++)
				{				
					for(int j=0;j<MAXPAR;j++)	//20
					{
						delete [] PTable[i].cmd[j];	//delete 2nd dimension:MAXPARLEN
					}
					//delete [] PTable[i].cmd;	//delete 1st dimension:MAXPAR
				}

			}
		}//end of fd for loop
	}//end of while


	return 0;
}

void initialize_path()	//Initialize the program directory
{
	char pro_path[MAXPATH];
	getcwd(pro_path,MAXPATH);	//get current directory path
	strcat(pro_path,"/ras/");	//add "/ras/"
	chdir(pro_path); 	//initialize the program directory
	chroot(pro_path);	//initialize defalt root PATH

	clearenv();	//remove all env variable
	setenv("PATH","bin:.",1);	//initial PATH variable is bin/ and ./
}
void send_welcome_msg(int sockfd)
{
	char welcome_msg[300];
	memset(welcome_msg,0,sizeof(welcome_msg));	//clean every time
	strcpy(welcome_msg,"****************************************\n");
	strcat(welcome_msg,"** Welcome to the information server. **\n");
	strcat(welcome_msg,"****************************************\n");
	if(write(sockfd,welcome_msg,strlen(welcome_msg))<0)
	{cout<<"Send welcome_msg error..."<<endl;}
	else
	{cout<<"Send welcome_msg correct..."<<endl;}
	//memset(welcome_msg,0,sizeof(welcome_msg));	//clean every time	
}
void get_conn_msg(int sockfd,char * user_name,char * user_ip,int user_port)
{
	char conn_msg[100];
	char port[10];
	sprintf(port,"%d",user_port);

	strcpy(conn_msg,"*** User '");
	strcat(conn_msg,user_name);
	strcat(conn_msg,"' entered from ");
	strcat(conn_msg,user_ip);
	strcat(conn_msg,"/");
	strcat(conn_msg,port);
	strcat(conn_msg,". ***\n");
	//write(sockfd,conn_msg,strlen(conn_msg));
	//return conn_msg;
	//memset(conn_msg,0,sizeof(conn_msg));	//clean every time	
}
void send_symbol(int sockfd)
{
	char symbol[2];
	memset(symbol,0,sizeof(symbol));	//clean every time
	strcpy(symbol,"% ");
	write(sockfd,symbol,strlen(symbol));
	//memset(symbol,0,sizeof(symbol));	//clean every time
}
int readline(int fd, char * ptr,int maxlen)
{
  int n,rc;
  char c;
  for(n=1;n<maxlen;n++)
  {
    if ((rc=read(fd,&c,1))==1)
    {
      	*ptr++=c;
      	if(c=='\n')
      	{
      		break;
      	}
    }
    else if(rc==0)
    {
      	if(n==1){return(0);}  //EOF, no data read
      	else {break;} //EOF, some data was read
    }
    else
    {
    	return(-1);
    }
  }
  *ptr=0;
  return(n);	//return length
}
/*
void set_pipe()
{
	if(getrlimit(RLIMIT_NOFILE,&r)<0)
	{
		fprintf(stderr,"getrlimit error\n");
		exit(1);
	}
	printf("RLIMIT_NOFILE cur:%d\n",r.rlim_cur);
	printf("RLIMIT_NOFILE max:%d\n",r.rlim_max);

	// set limit
	r.rlim_cur = 2200;
	r.rlim_max = 3000;
	if (setrlimit(RLIMIT_NOFILE,&r)<0)
	{
		fprintf(stderr,"setrlimit error\n");
		exit(1);
	}
}
*/
void signal_handler(int signum)	//kill zombie
{
	//cout<<"Interrupt signal ("<<signum<<") received.\n";
	exit(signum);
}
void broadcast(int * user_fd,int tablesize,char * msg)
{
	for(int i=0;i<tablesize;i++)
	{
		if(user_fd[i]!=-1)	//not null
		{
			write(user_fd[i],msg,strlen(msg));
		}
	}
}
void write2(int sockfd,char *msg)
{
	write(sockfd,msg,strlen(msg));
}
char* int2char(int i)
{
	char str[10];
	memset(str,0,sizeof(str));
	sprintf(str,"%d",i);

	return str;
}

/*ppt page.16 start
	service: associated with the desired port.
	protocol: name of protocol to use, "tcp" or "udp".
	qlen: maximum length of the server request queue.
*/
int passivesock(char * service,char * protocol,int qlen)
{
	struct servent *pse;	//service information entry
	struct protoent *ppe;	//protocol information entry
	struct sockaddr_in sin;	//internet endpoint address
	int s,type;
	unsigned short portbase=0;	//for non-root server
	
	bzero((char *)&sin,sizeof(sin));
	sin.sin_family=AF_INET;
	sin.sin_addr.s_addr=INADDR_ANY;
	//Map service name to port number
	if(pse=getservbyname(service,protocol))
	{
		sin.sin_port=htons(ntohs((u_short)pse->s_port)+portbase);
	}
	else if((sin.sin_port=htons((u_short)atoi(service)))==0)
	{
		cout<<"Can't get "<<service<<" service entry\n";
		exit(1);
	}
	//Map protocol name to protocol number
	if((ppe=getprotobyname(protocol))==0)
	{
		cout<<"Can't get "<<protocol<<" protocol entry\n";
		exit(1);
	}
	//Use protocol to choose a socket type
	if(strcmp(protocol,"udp")==0) {type=SOCK_DGRAM;}
	else{type=SOCK_STREAM;}	//tcp
	//Allocate a socket
	s=socket(PF_INET,type,ppe->p_proto);
	if(s<0)
	{
		cout<<"Can't create socket\n";
		exit(1);
	}
	//Bind the socket
	if(bind(s,(struct sockaddr *)&sin,sizeof(sin))<0)
	{
		cout<<"Can't bind to "<<service<<"\n";
		exit(1);
	}
	else{cout<<"Server bind success, open...\n";}
	if(type==SOCK_STREAM && listen (s,qlen)<0)
	{
		cout<<"Can't listen on "<<service<<"\n";
		exit(1);
	}
	return s;
}
